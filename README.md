# README

Notes
- I have added dotenv-rails to the project, this means that database credentials can be stored in a .env file, this kind of thing shouldn't be in source control. See .env.example for an example to copy into .env on your environment
- I used a mysql database in development, mostly because I have a client set up to read from my development mysql instance, could just have easily been sqlite, and probably should put that in the .env as well
- devise is installed to handle user authentication, see seeds.rb for the seeded users
- I've not brought in a frontend javascript technology, just done classic rails .erb files for this simple test task

- I've used rspec in the past for ruby testing, but have just used the built in rails testing facilities here to take a look at them
- The test that test validation are testing the framework more than anything else to be honest and I'm ambivalent about including these tests
- Order.transaction_type could have been used via an enum, however this stores integers in the database which makes it less clear for ad-hoc queries

- I've let the database do the heavy lifting on working out the summary data, that's usually the sensible approach
- The order summary methods could return a collection of objects of a defined class, and been in their own class, rather than just as class methods of the Order model, but I didn't seem necessary for this small exercise

- The order page should paginate, anything that doesn't won't scale properly, I'd have added if there was more time
- I haven't added front end tests, again, this was a time issue, I didn't want to spend too long on the task, I'm spending a fair amount of time refreshing my rails skills as part of this exercise
- I've used non-responsive html tables within the views, rather than spend time looking into adding something like bootstrap or tailwind to aid in responsiveness
- I've just used rails scaffolding to generate the controllers and views
- I've blanket protected the entire application for login by adding a check into the application controller
- The buy and sell summary tables could have been in partials, as could the index table
- There is no internationalisation / multi language support, again due to time constraints

- I've used ChatGPT as a 'wingman' in this project, very useful tool!  The order form was written by ChatGPT, it gave CSS classes to the div's that aren't really relevant to this project, but I've left them in in case I added bootstrap later.


