class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.references :user, null: false, foreign_key: true
      t.decimal :quantity, precision: 12, scale: 2
      t.decimal :price_per_kg, precision: 12, scale: 2
      t.string :transaction_type

      t.timestamps
    end
  end
end
