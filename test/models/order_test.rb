require "test_helper"

class OrderTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "cannot save without required fields" do

    order = Order.new
    assert_not order.save

    user = User.first

    order.user = User.first
    assert_not order.save

    order.quantity = 1
    assert_not order.save

    order.price_per_kg = 302.00
    assert_not order.save

    order.transaction_type = Order::BUY_TRANSACTION_TYPE
    assert order.save
  end

  test "quantity must be greater than 0" do
    order = Order.first
    order.quantity = 0
    assert_not order.save
    order.quantity = -1
    assert_not order.save
    order.quantity = 1
    assert order.save
  end

  test "price_per_kg must be greater than or equal to 0" do
    order = Order.first
    order.price_per_kg = -1
    assert_not order.save
    order.price_per_kg = 0
    assert order.save
    order.price_per_kg = 1
    assert order.save
  end

  test "transaction type must be valid" do 
    order = Order.first     
    order.transaction_type = 'NOT BUY OR SELL'
    assert_not order.save
    assert_equal(['Transaction type NOT BUY OR SELL is not a valid transaction type'], order.errors.full_messages_for(:transaction_type))

    order.transaction_type = Order::BUY_TRANSACTION_TYPE
    assert order.save

    order.transaction_type = Order::SELL_TRANSACTION_TYPE
    assert order.save

  end 

  def create_summary_price_test_data
    user1 = User.first
    user2 = User.second
    user3 = User.third
    user4 = User.last

    Order.delete_all
    Order.create([
                  {:user => user1, :quantity => 3.5, :price_per_kg => 306.00, :transaction_type => Order::SELL_TRANSACTION_TYPE },
                  {:user => user2, :quantity => 1.2, :price_per_kg => 310.00, :transaction_type => Order::SELL_TRANSACTION_TYPE },
                  {:user => user3, :quantity => 1.5, :price_per_kg => 307.00, :transaction_type => Order::SELL_TRANSACTION_TYPE },
                  {:user => user4, :quantity => 2.0, :price_per_kg => 306.00, :transaction_type => Order::SELL_TRANSACTION_TYPE },
                  {:user => user1, :quantity => 3.5, :price_per_kg => 306.00, :transaction_type => Order::BUY_TRANSACTION_TYPE },
                  {:user => user2, :quantity => 1.2, :price_per_kg => 310.00, :transaction_type => Order::BUY_TRANSACTION_TYPE },
                  {:user => user3, :quantity => 1.5, :price_per_kg => 307.00, :transaction_type => Order::BUY_TRANSACTION_TYPE },
                  {:user => user4, :quantity => 2.0, :price_per_kg => 306.00, :transaction_type => Order::BUY_TRANSACTION_TYPE }
                ])
  end

  def burn_summary_price_test_data
    Order.delete_all
    # reinstate the order fixtures
    ActiveRecord::FixtureSet.create_fixtures(Rails.root.join('test', 'fixtures'), 'orders')    
  end

  test "summary_prices_per_price_point_for_sale contains correct data" do

    create_summary_price_test_data
    assert_equal([{:quantity => 5.5, :price_per_kg => 306},
                  {:quantity => 1.5, :price_per_kg => 307},
                  {:quantity => 1.2, :price_per_kg => 310},
                ],Order.summary_prices_per_price_point_for_sale)
    burn_summary_price_test_data
  end

  test "summary_prices_per_price_point_for_buy contains correct data" do
    create_summary_price_test_data
    assert_equal([{:quantity => 1.2, :price_per_kg => 310},
                  {:quantity => 1.5, :price_per_kg => 307},
                  {:quantity => 5.5, :price_per_kg => 306},
                ],Order.summary_prices_per_price_point_for_buy)
    burn_summary_price_test_data
  end

end
