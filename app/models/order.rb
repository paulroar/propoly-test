class Order < ApplicationRecord
  BUY_TRANSACTION_TYPE = 'BUY'
  SELL_TRANSACTION_TYPE = 'SELL'

  belongs_to :user
  validates :user, presence: true
  validates :transaction_type, inclusion: { in: [Order::BUY_TRANSACTION_TYPE, Order::SELL_TRANSACTION_TYPE ], message: "%{value} is not a valid transaction type" }, presence: true
  validates :price_per_kg, numericality: {  greater_than_or_equal_to: 0 }, presence:true
  validates :quantity,numericality: {  greater_than: 0 }, presence:true



  def self.summary_prices_per_pricepoint(transaction_type, order)
    Order.where(transaction_type: transaction_type)
         .group(:price_per_kg).order(price_per_kg: order)
         .pluck('sum(quantity) as total_quantity','price_per_kg')
         .map { |total_quantity, price_per_kg| {quantity: total_quantity, price_per_kg: price_per_kg} }

  end

  def self.summary_prices_per_price_point_for_sale
    summary_prices_per_pricepoint(Order::SELL_TRANSACTION_TYPE,:asc)
  end

  def self.summary_prices_per_price_point_for_buy
    summary_prices_per_pricepoint(Order::BUY_TRANSACTION_TYPE,:desc)
  end
end
